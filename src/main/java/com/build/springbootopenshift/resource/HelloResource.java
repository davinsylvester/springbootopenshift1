package com.build.springbootopenshift.resource;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/") //http://localhost:8080/
public class HelloResource {

    @GetMapping
    public String hello(){
        return "Hello there! This is a springboot project running in Openshift";
    }

}
