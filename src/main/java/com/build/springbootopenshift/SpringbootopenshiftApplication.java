package com.build.springbootopenshift;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringbootopenshiftApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringbootopenshiftApplication.class, args);
	}

}
